package C2_UD20_T20_7.C2_UD20_T20_7;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class mainApp extends JFrame {

	private JPanel contentPane;
	private JTextField inputTextField;
	private JTextField resultTextField;

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				
				try {
					
					mainApp frame = new mainApp();
					frame.setVisible(true);
					
				} catch (Exception e) {
					
					e.printStackTrace();
					
				}
				
			}
			
		});
		
	}

	public mainApp() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 180);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		JLabel mainLabel = new JLabel("Cantidad a convertir");
		sl_contentPane.putConstraint(SpringLayout.NORTH, mainLabel, 13, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, mainLabel, -291, SpringLayout.EAST, contentPane);
		contentPane.add(mainLabel);
		
		inputTextField = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, inputTextField, -3, SpringLayout.NORTH, mainLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, inputTextField, 6, SpringLayout.EAST, mainLabel);
		sl_contentPane.putConstraint(SpringLayout.EAST, inputTextField, -194, SpringLayout.EAST, contentPane);
		contentPane.add(inputTextField);
		inputTextField.setColumns(10);
		
		JLabel resultLabel = new JLabel("Resultado");
		sl_contentPane.putConstraint(SpringLayout.NORTH, resultLabel, 0, SpringLayout.NORTH, mainLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, resultLabel, 6, SpringLayout.EAST, inputTextField);
		sl_contentPane.putConstraint(SpringLayout.EAST, resultLabel, -126, SpringLayout.EAST, contentPane);
		contentPane.add(resultLabel);
		
		resultTextField = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, resultTextField, -3, SpringLayout.NORTH, mainLabel);
		sl_contentPane.putConstraint(SpringLayout.WEST, resultTextField, 6, SpringLayout.EAST, resultLabel);
		sl_contentPane.putConstraint(SpringLayout.EAST, resultTextField, -29, SpringLayout.EAST, contentPane);
		resultTextField.setEditable(false);
		contentPane.add(resultTextField);
		resultTextField.setColumns(10);
		
		final JButton convertButton = new JButton("Euros a ptas");
		contentPane.add(convertButton);
		
		JButton swapButton = new JButton("Cambiar");
		sl_contentPane.putConstraint(SpringLayout.WEST, swapButton, 328, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, swapButton, -10, SpringLayout.EAST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.NORTH, convertButton, 0, SpringLayout.NORTH, swapButton);
		sl_contentPane.putConstraint(SpringLayout.EAST, convertButton, -6, SpringLayout.WEST, swapButton);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, swapButton, -10, SpringLayout.SOUTH, contentPane);
		contentPane.add(swapButton);
		
		convertButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				if (convertButton.getText().equalsIgnoreCase("Euros a ptas")) {
					
					double parsedNumber = Double.parseDouble(inputTextField.getText());
					double resultParsedNumber = parsedNumber * 166.386;
					
					resultTextField.setText(String.valueOf(resultParsedNumber));
					
				} else {
					
					double parsedNumber = Double.parseDouble(inputTextField.getText());
					double resultParsedNumber = parsedNumber / 166.386;
					
					resultTextField.setText(String.valueOf(resultParsedNumber));
					
				}
				
			}
			
		});
		
		swapButton.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				
				if (convertButton.getText().equalsIgnoreCase("Euros a ptas")) {
					
					convertButton.setText("Ptas a euros");
					
				} else {
					
					convertButton.setText("Euros a ptas");
					
				}
				
			}
			
		});
		
	}
	
}
